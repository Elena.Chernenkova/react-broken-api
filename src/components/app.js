import React, { Component } from "react";
import DevilApiService from "../services/devil-api-service";
import LoginPage from "./login-page";
import Chart from "./Chart";

export default class App extends Component {
    state = {
        isLoggedIn: false,
        service: new DevilApiService()
    }

  loggedIn = (logged) => {
      this.setState({
          isLoggedIn: logged
      })
  }

  render () {
      const { service, isLoggedIn } = this.state;
      const mainComponent =  !isLoggedIn
          ? <LoginPage service={service} loggedIn={this.loggedIn} />
          : <Chart service={service} loggedIn={this.loggedIn} />;
      return (
          <div style={{
              height: document.body.clientHeight - 10,
              width: document.body.clientWidth - 10 }} >
              { mainComponent }
          </div>
      )
  }
}
