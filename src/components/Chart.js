import React, { Component } from "react";
import { ResponsiveLineCanvas } from '@nivo/line';
import { connect } from "react-redux";
import * as actions from "../actions";
import { Empty } from 'antd';

class Chart extends Component {
    state: {
        data: undefined,
        dmin: undefined,
        dmax: undefined,
        vmin: undefined,
        vmax: undefined,
        loaded: false,
        interval: undefined
    }

    componentDidMount = () => {
        this.getChartData();
        const interval = setInterval(this.getChartData, 5000);
        this.setState({
            interval
        })
    }

    componentWillUnmount() {
        clearInterval(this.state.interval);
    }

    getChartData = () => {
        const {chart, service, loggedIn} = this.props;
        chart(service, this.dataReceived, loggedIn);
    }

    dataReceived = ({ data, dmin, dmax, vmin, vmax }) => {
        if (data && dmin && dmax && vmin && vmax && dmax !== dmin && vmax !== vmin) {
            this.setState({
                data,
                dmin, dmax, vmin, vmax,
                loaded: true
            });
        } else {
            this.setState({
                loaded: false
            });
        }
    }


    render() {
        if (this.state?.loaded) {
            const { data, dmin, dmax, vmin, vmax } = this.state;
            return (
                <ResponsiveLineCanvas
                    data={data}
                    margin={{top: 50, right: 60, bottom: 100, left: 80}}
                    xScale={{type: 'linear', min: dmin, max: dmax}}
                    yScale={{type: 'linear', min: vmin, max: vmax}}
                    yFormat=" >-.2f"
                    isInteractive={false}
                    curve="monotoneX"
                    axisBottom={{
                        type: 'time',
                        legend: 'date',
                        legendPosition: 'middle',
                        legendOffset: 40
                    }}
                    axisLeft={{
                        format: '.2s',
                        legend: 'value',
                        legendPosition: 'middle',
                        legendOffset: -50
                    }}
                    enableGridX={false}
                    colors={{scheme: 'set1'}}
                    lineWidth={1}
                    pointSize={3}
                    pointColor={{theme: 'background'}}
                    pointBorderWidth={1}
                    pointBorderColor={{from: 'serieColor'}}
                    pointLabelYOffset={-12}
                    useMesh={true}
                    legends={[
                        {
                            anchor: 'bottom',
                            direction: 'row',
                            justify: false,
                            translateY: 70,
                            itemsSpacing: 2,
                            itemDirection: 'left-to-right',
                            itemWidth: 30,
                            itemHeight: 12,
                            itemOpacity: 0.75,
                            symbolSize: 12,
                            symbolShape: 'circle',
                            symbolBorderColor: 'rgba(0, 0, 0, .5)'
                        }
                    ]}
                />
            )
        } else {
            return <Empty style={{
                position: 'fixed',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)'}}
                description={
                    <span>Подождите, идет загрузка данных</span>
                } />;
        }
    }
}

export default connect(null, actions)(Chart);