import React from "react";
import { connect } from 'react-redux';
import * as actions from '../actions';
import { Form, Input, Button, Radio } from 'antd';

const LoginPage = (props) => {
    const [mode, setMode] = React.useState('log');
    const { service, loggedIn, login, register } = props;

    const onFinish = (values: any) => {
        if (mode === 'log') {
            login(values, service, loggedIn);
        } else {
            register(values, service);
        }
    };

    const toggleMode = ({ target }) => {
        setMode(target.value);
    };

    return (
        <div style={{
            width: '25%',
            margin: 'auto',
            paddingTop: '7%'
        }}>
            <div style={{
                paddingBottom: '10%'
            }}>
                <Radio.Group defaultValue="log" buttonStyle="solid" onChange={toggleMode}>
                    <Radio.Button value="log">Войти</Radio.Button>
                    <Radio.Button value="reg">Зарегистрироваться</Radio.Button>
                </Radio.Group>
            </div>
            <Form
                layout='vertical'
                name="basic"
                onFinish={onFinish}
            >
                <Form.Item
                    label="Логин"
                    name="user"
                    rules={[{required: true, message: 'Введите более 3 символов', min: 4}]}
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                    label="Пароль"
                    name="password"
                    rules={[{required: true, message: 'Введите более 3 символов', min: 4}]}
                >
                    <Input.Password/>
                </Form.Item>


                <Form.Item style={{
                    paddingTop: '7%'
                }}>
                    <Button type="primary" htmlType="submit">
                        Продолжить
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
}

export default connect(null, actions)(LoginPage);
