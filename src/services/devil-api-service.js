export default class DevilApiService {
    _apiBase = 'http://devil-api.devlit.tech/api/';
    _headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    _apiLogin = 'login';
    _apiRegister = 'register';
    _apiLogout = 'logout';
    _apiData = 'data';


    async _sendPostRequest(url, body = {}) {
        const res = await fetch(`${this._apiBase}${url}`,
            {
                method: 'POST',
                credentials: 'include',
                body: new URLSearchParams(body),
                headers: this._headers
            });
        return await res.json();
    }

    async _sendGetRequest(url) {
        const res = await fetch(`${this._apiBase}${url}`,
            {
                method: 'GET',
                credentials: 'include',
                headers: this._headers
            });
        return await res.json();
    }

    async login(data) {
        const res = await this._sendPostRequest(this._apiLogin, data);
        if (res.length && res[0].name === data.user) {
            return true;
        }
        alert(res.message);
        return false;
    }

    async register({ user, password }) {
        const res = await this._sendPostRequest(this._apiRegister, { name: user, password });
        if (res.status === 'ok') {
            alert('Успешно! Войдите в систему');
            return true;
        }
        alert('Регистрация не пройдена. Попробуйте еще раз');
        return false;
    }

    async logout() {
        const res = await this._sendGetRequest(this._apiLogout);
    }

    async getData() {
        const res = await this._sendGetRequest(this._apiData);
        if (res) {
            return res;
        }
        return false;
    }
}
