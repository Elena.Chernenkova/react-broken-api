const reducer = (state = null, action) => {
    switch (action.type) {
        case 'login':
            return action.payload;

        case 'register':
            return action.payload;

        case 'chart':
            return action.payload;

        default:
            return action.payload;
    }
};

export default reducer;
