export const login = (data, service, loggedIn) => {
    service.login(data).then((response) => {
        loggedIn(response);
    });
    return {
        type: 'login',
        payload: data
    }
};

export const register = (data, service) => {
    service.register(data);
    return {
        type: 'register',
        payload: data
    }
};

export const chart = (service, dataReceived, loggedIn) => {
    service.getData().then((response) => {
        if (response.statusCode === 401) {
            loggedIn(false);
        }
        if(!response) {
            return;
        }
        const data = _proceedData(response);
        dataReceived(data);
    })
    return {
        type: 'chart'
    };
}

const _proceedData = (response) => {
    let dates = [], values = [];
    const data = response?.graphsArray?.map((x, index) => {
        try {
            return {
                id: index + 1,
                data: x.data.map((t) => {
                    try {
                        if (typeof t.value === 'number') {
                            const newDate = Date.parse(t.date);
                            dates.push(newDate);
                            values.push(t.value);
                            return {x: newDate, y: t.value}
                        }
                    } catch (e) {
                        return null
                    }
                }).filter(x => x != null)
            }
        } catch (e) {
            return null;
        }
    }).filter(x => x != null);
    return {
        data,
        dmin: dates.length > 0 ? Math.min(...dates) : undefined,
        dmax: dates.length > 0 ? Math.max(...dates) : undefined,
        vmin: values.length > 0 ? Math.min(...values) : undefined,
        vmax: values.length > 0 ? Math.max(...values) : undefined

    }
}
