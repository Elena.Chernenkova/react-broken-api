# UI for Devil API Server

This project includes registration and authorization in the system which shows the lifecycle of the chart.

The chart is based on data from Devil API Server:  
https://documenter.getpostman.com/view/3073008/SzKbLF5y  
Server can send broken data or do logout.

The chart is built with the lib:  
https://nivo.rocks/line/canvas/

To receive data correctly it is necessary to set flag in browser.  
Go to chrome://flags/ and disable 'SameSite by default cookies' flag.